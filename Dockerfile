FROM ubuntu:latest
MAINTAINER Alberto Lietor "alietors@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /pastedoc
WORKDIR /pastedoc
RUN pip install flask
RUN pip install elasticsearch
RUN pip install pyyaml
ENTRYPOINT ["python"]
CMD ["main.py"]