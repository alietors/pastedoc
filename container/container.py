from elasticsearch import Elasticsearch
from db.db_wrapper import DbWrapper
import yaml


class DiContainer():

    def __init__(self):
        with open('config/config.yml', 'r') as yml:
            config = yaml.load(yml)

        self.di_container = {
            "db_wrapper": DbWrapper(
                Elasticsearch(
                    ['http://{}:{}@{}:{}'.format(config['db']['user'], config['db']['password'], config['db']['url'], config['db']['port'])]
                )
            )
        }

    def get(self, component):
        return self.di_container[component]