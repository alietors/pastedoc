from elasticsearch import Elasticsearch
from model.document import Document
import uuid


class DbWrapper():
    def __init__(self, es):
        self.es = es
           # Elasticsearch(['http://elastic:changeme@localhost:9200'])

    def add_document(self, document):
        id = uuid.uuid4()
        self.es.index(index="pastedoc", doc_type="document", id=id, body={
            "title": document.title,
            "body": document.body,
            "readonly": document.readonly
        })
        return id

    def update_document(self, document):
        self.es.index(index="pastedoc", doc_type="document", id=document.id, body={
            "title": document.title,
            "body": document.body,
            "readonly": document.readonly
        })
        return document.id

    def get_document(self, id):
        result = self.es.get(index="pastedoc", doc_type="document", id=id)['_source']
        return Document(title=result['title'], body=result['body'], readonly=result['readonly'], id=id)
