# pastedoc

Trying to do something similar to pastebin totally anonymous, I just want to play around and learn
some python and flask.

## Run application
docker-compose up

## Stop application
docker-compose down

## Configuration
files:

    config/config.yml
    config/elastic_config.yml
    
## Troubleshooting
sudo sysctl -w vm.max_map_count=262144