from flask import Flask, render_template, request, jsonify
from model.document import Document
from container import DiContainer


def create_app():
    app = Flask(__name__)
    return app

pastedoc = create_app()
di_container = DiContainer()
dbWrapper = di_container.get("db_wrapper")


@pastedoc.route("/")
def home():
    return render_template("index.html")


@pastedoc.route("/document", methods = ["POST"])
def create_document():
    title = request.form['title']
    body = request.form['tinydata']
    readonly = request.form['readonly'] == 'true'

    doc = Document(title=title, body=body, readonly=readonly, id=0)
    id = dbWrapper.add_document(doc)
    doc.id = id

    return jsonify(id=doc.id)


@pastedoc.route("/document/<string:id>", methods = ["PUT"])
def update_document(id):
    title = request.form['title']
    body = request.form['tinydata']
    readonly = request.form['readonly'] == 'true'

    doc = Document(title=title, body=body, readonly=readonly, id=id)
    dbWrapper.update_document(doc)

    return jsonify(id=doc.id)


@pastedoc.route("/document/<string:id>", methods = ["GET"])
def get_document(id):
    document = dbWrapper.get_document(id)

    if document.readonly:
        return render_template("document.html", title=document.title, body=document.body)
    else:
        return render_template("edit.html", title=document.title, body=document.body, readonly=document.readonly)

if __name__ == "__main__":
    pastedoc.run(debug=True)